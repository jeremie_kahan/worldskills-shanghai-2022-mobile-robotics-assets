# WorldSkills Shanghai 2022 Mobile Robotics assets

This repository regroups a set of assets developed for the WorldSkills "Mobile Robotics" competition.

Authors:
- Jérémie Kahan (Mobile Robotics referee & coach)
- Lucas Si Larbi (competitor)
- Antoine Gros (competitor)